#include <iostream>
#include <string>

#include "SteppingAction.hh"

#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "G4VPhysicalVolume.hh"

SteppingAction::SteppingAction() : G4UserSteppingAction()
{
}

void SteppingAction::UserSteppingAction(const G4Step* /*aStep*/)
{
  //const G4StepPoint* start_point = aStep->GetPreStepPoint();
  //const G4VPhysicalVolume* phys_vol = start_point->GetPhysicalVolume();
  //const std::string& vol_name = phys_vol->GetName();
  //const G4ThreeVector& pos = start_point->GetPosition();

  //std::cout << "Particle now in " << vol_name << " at ("
  //  << pos.x() << ", " << pos.y() << ", " << pos.z() << ")\n";
}

#include <iostream>

#include "TrackingAction.hh"

#include "G4Neutron.hh"
#include "G4RunManager.hh"
#include "G4Step.hh"
#include "G4SystemOfUnits.hh"
#include "G4Track.hh"
#include "G4VProcess.hh"

#include "g4root.hh" // Use ROOT format histograms for G4 analysis

TrackingAction::TrackingAction() : G4UserTrackingAction()
{
}

void TrackingAction::PreUserTrackingAction(const G4Track* /*aTrack*/)
{
  //static unsigned long long track_count = 0;
  //if (track_count % 100 == 0) std::cout << "Beginning track #" << track_count
  //  << '\n';
  //++track_count;
}

void TrackingAction::PostUserTrackingAction(const G4Track* aTrack)
{
  const G4Step* step = aTrack->GetStep();
  if (!step) return;

  const G4ParticleDefinition* particle_def = aTrack->GetDynamicParticle()
    ->GetDefinition();

  // Only count neutrons
  if ( particle_def != G4Neutron::Definition() ) return;

  // This pointer will be nullptr if the track was killed when
  // it went out-of-world.
  const G4VPhysicalVolume* post_vol = step->GetPostStepPoint()
    ->GetPhysicalVolume();
  if (post_vol) return;
  else ++num_escaped_;

  std::cout << num_escaped_ << " have escaped!\n";

  //// fill histograms
  //G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  //double tCapture = (end_time_ - start_time_) / ms;
  //analysisManager->FillH1(0, tCapture);
  //analysisManager->FillNtupleDColumn(0, tCapture);
  //analysisManager->AddNtupleRow();
  //int ev_num = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  //if (ev_num % 100 == 0) G4cout << "event " << ev_num
  //  << ": capture time = " << tCapture << " ms" << G4endl;
}

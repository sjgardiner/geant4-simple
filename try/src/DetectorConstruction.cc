// Geant4 includes
#include "G4SystemOfUnits.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4SubtractionSolid.hh"
#include "G4PVPlacement.hh"

// My includes
#include "DetectorConstruction.hh"

DetectorConstruction::DetectorConstruction()
{
}

G4VPhysicalVolume* DetectorConstruction::Construct() {

  auto* material_manager = G4NistManager::Instance();

  G4Box* outer_box = new G4Box("Outer box", 100.*cm,
    100.*cm, 100.*cm);
  G4Box* inner_box = new G4Box("Inner box", 99.99*cm,
    90.*cm, 90.*cm);
  G4SubtractionSolid* hollow_box = new G4SubtractionSolid(
    "Hollow box", outer_box, inner_box);

  G4Material* polyethylene
    = material_manager->FindOrBuildMaterial("G4_POLYETHYLENE");

  // pure boron
  G4Material* boron
    = material_manager->FindOrBuildMaterial("G4_B");

  auto* borated_polyethylene = new G4Material("BoratedPoly05", /*density*/ 1.06*g/cm3,
    /* number of components */ 2);
  borated_polyethylene->AddMaterial(boron, 0.05);
  borated_polyethylene->AddMaterial(polyethylene, 0.95);

  auto* air = material_manager->FindOrBuildMaterial("G4_AIR");

  G4LogicalVolume* logical_world = new G4LogicalVolume( outer_box,
    air, air->GetName() );

  G4LogicalVolume* logical_hollow_box = new G4LogicalVolume( hollow_box,
    borated_polyethylene, borated_polyethylene->GetName() );

  [[maybe_unused]] G4VPhysicalVolume* my_box = new G4PVPlacement(
    0,  // no rotation
    G4ThreeVector(),    // at (0, 0, 0)
    logical_hollow_box, // its logical volume
    "MyBox",            // its name
    logical_world,      // its mother logical volume
    false,              // no boolean operation
    0);                 // copy number

  world_ = new G4PVPlacement(0, G4ThreeVector(), logical_world, "WorldBox",
    nullptr, false, 0);

  return world_;
}

const G4VPhysicalVolume* DetectorConstruction::GetWorld() const {
  return world_;
}

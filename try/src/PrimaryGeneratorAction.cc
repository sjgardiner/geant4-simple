#include <iostream>

#include "PrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4PhysicalConstants.hh"
#include "G4PrimaryParticle.hh"
#include "G4PrimaryVertex.hh"
#include "G4SystemOfUnits.hh"
#include "G4Neutron.hh"
#include "G4Geantino.hh"
#include "Randomize.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction()
{
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  G4PrimaryVertex* vertex = new G4PrimaryVertex(0., 0., 0., 0.); // x,y,z,t0

  // Choose cos(theta) uniformly on [-1, 1]
  double cos_theta = -1. + 2.*G4UniformRand();
  double sin_theta = std::sqrt( 1. - std::pow(cos_theta, 2) );

  // Choose phi uniformly on [0, 2*pi]
  double phi = 0. + 2*CLHEP::pi*G4UniformRand();

  // TODO: sample kinetic energies from AmBe source distribution
  double kinetic_energy = 1.*MeV;

  G4ParticleDefinition* neutron_def = G4Neutron::Definition();
  double mass = neutron_def->GetPDGMass();
  double total_energy = kinetic_energy + mass;

  double p_tot = std::sqrt( std::max(std::pow(total_energy, 2)
    - std::pow(mass, 2), 0.) );

  G4PrimaryParticle* particle = new G4PrimaryParticle( G4Neutron::Definition(),
    p_tot * sin_theta * std::sin(phi),
    p_tot * sin_theta * std::cos(phi),
    p_tot * cos_theta);

  vertex->SetPrimary(particle);

  anEvent->AddPrimaryVertex(vertex);
}
